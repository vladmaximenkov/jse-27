package ru.vmaksimenkov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.model.User;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Domain implements Serializable {

    @Nullable
    private List<Project> projects;

    @Nullable
    private List<Task> tasks;

    @Nullable
    private List<User> users;

}
