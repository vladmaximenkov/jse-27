package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;

public final class TaskByIndexSetStatusCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Set task status by index";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber();
        if (!checkIndex(index, serviceLocator.getTaskService().size(userId))) throw new IndexIncorrectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getTaskService().setTaskStatusByIndex(userId, index, Status.getStatus(TerminalUtil.nextLine()));

    }

    @NotNull
    @Override
    public String name() {
        return "task-set-status-by-index";
    }

}
