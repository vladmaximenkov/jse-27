package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Sort;
import ru.vmaksimenkov.tm.exception.entity.SortNotFoundException;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public final class TaskListSortCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list sorted";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST SORTED]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable List<Task> list;
        try {
            list = serviceLocator.getTaskService().findAll(userId, Sort.getSort(TerminalUtil.nextLine().toUpperCase()).getComparator());
        } catch (@NotNull final SortNotFoundException e) {
            System.err.println(e.getMessage() + " Default sort instead");
            list = serviceLocator.getTaskService().findAll(userId);
        }
        if (list == null) return;
        System.out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT");
        @NotNull AtomicInteger index = new AtomicInteger(1);
        list.forEach((x) -> System.out.println(index.getAndIncrement() + "\t" + x));
    }

    @NotNull
    @Override
    public String name() {
        return "task-list-sort";
    }

}
