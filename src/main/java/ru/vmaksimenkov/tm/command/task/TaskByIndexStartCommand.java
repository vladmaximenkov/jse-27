package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByIndexStartCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start task by index";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getTaskService().startTaskByIndex(userId, TerminalUtil.nextNumber());
    }

    @NotNull
    @Override
    public String name() {
        return "task-start-by-index";
    }

}
