package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class ProjectByNameFinishCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish project by name";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        serviceLocator.getProjectService().finishProjectByName(userId, TerminalUtil.nextLine());
    }

    @NotNull
    @Override
    public String name() {
        return "project-finish-by-name";
    }

}
