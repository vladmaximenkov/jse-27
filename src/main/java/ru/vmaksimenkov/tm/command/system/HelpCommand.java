package ru.vmaksimenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @Nullable final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        assert commands != null;
        commands.forEach(e -> System.out.println(e.name() + ": " + e.description()));
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

}