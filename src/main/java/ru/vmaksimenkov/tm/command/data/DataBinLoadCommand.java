package ru.vmaksimenkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Load binary data from file";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BIN LOAD]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @NotNull
    @Override
    public String name() {
        return "data-bin-load";
    }

}
