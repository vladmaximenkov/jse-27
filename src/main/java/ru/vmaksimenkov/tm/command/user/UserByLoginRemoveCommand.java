package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Role;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

public final class UserByLoginRemoveCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        serviceLocator.getUserService().removeByLogin(nextLine());
    }

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
