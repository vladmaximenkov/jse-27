package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getAuthService().getUser();
        if (user == null) return;
        showUser(user);
    }

    @NotNull
    @Override
    public String name() {
        return "user-view";
    }

}
